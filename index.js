const express = require("express");
const mysql = require("mysql");
const bodyparser = require("body-parser");
let config = {
    "host": "localhost",
    "user": "root",
    "password":"",
    "database":"oee_exc"
};

let app = express();
app.use(bodyparser.json());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,PUT,PATCH,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();

});
var con = mysql.createConnection(config);
app.get('/actStatus', (req, res) => {

    con.connect(function(err) {
        let query = "SELECT * FROM  status ORDER BY id DESC LIMIT  1";
        con.query(query,(err,result)=>{
            if(err){
                console.log(err);
                res.status(500).json({message:"ERROR"});
            }
            else{
                res.status(200).json({success:true,status:result});
            }
        })
    });
});
app.get('/pieces', (req, res) => {
    con.connect(function(err) {
        let query = "SELECT * FROM  piecesall ORDER BY id DESC LIMIT  1";
        con.query(query,(err,result)=>{
            if(err){
                console.log(err);
                res.status(500).json({message:"ERROR"});
            }
            else{
                res.status(200).json({success:true,status:result});
            }
        })
    });
});
app.post('/oee', (req, res) => {
    let pieces;
    let defpieces;
    let day;
    let month;
    let year;

    let date = new Date(req.body.date);
    month = date.getMonth() + 1;
    day = date.getDate();
    year = date.getFullYear();
    console.log(date);
    console.log(day);
    console.log(month);
    console.log(year);
    con.connect(function(err) {
        let query = "SELECT * FROM  piecesall  where DAY(timestamp) = ? and MONTH(timestamp) = ? and YEAR(timestamp) = ? ORDER BY id DESC LIMIT 1";
        con.query(query,[day,month,year],(err,result)=>{
            if(err){
                console.log(err);
                res.status(500).json({message:"ERROR"});
            }
            else{
                if (result.length == 0) {
                    res.status(404).json({data:"No data found"});
                } else {
                    pieces = result[0].num;
                    let query2 = "SELECT * FROM  defpiecesall where DAY(timestamp) = ? and MONTH(timestamp) = ? and YEAR(timestamp) = ? ORDER BY id DESC LIMIT 1";
                    con.query(query2, [day, month, year], (err, result) => {
                        if (err) {
                            console.log(err);
                            res.status(500).json({ message: "ERROR" });
                        }
                        else {
                            if (result.length == 0) {
                                res.status(404).json({ data: "No data found" });;
                            } else {
                                defpieces = result[0].num;
                                let x = ((0.63 * (pieces - defpieces)) / 960);
                                res.status(200).json({ data: x });
                            }

                        }
                    })
                }
            }
        })
    });
});

app.listen(3000, () => {
    console.log('App listening on port 3000');
});